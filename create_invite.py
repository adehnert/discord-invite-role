#!/usr/bin/env python3

import asyncio
import logging
import sys

import discord

LOGGER = logging.getLogger(__name__)

async def create_invites(client, channel_id, reasons):
    channel = await client.fetch_channel(channel_id)
    for reason in reasons:
        invite = await channel.create_invite(reason=reason)
        LOGGER.info(f'{reason=} {invite=}')

if __name__ == '__main__':
    logging.basicConfig()
    LOGGER.setLevel(logging.DEBUG)
    client = discord.Client()
    with open('discord.token', 'r') as token_fp:
        token = token_fp.read().strip()
    @client.event
    async def on_ready():
        await create_invites(client, sys.argv[1], sys.argv[2:])
    loop = asyncio.get_event_loop()
    loop.run_until_complete(client.start(token))
