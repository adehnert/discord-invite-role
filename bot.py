#!/usr/bin/env python3

import csv
import yaml
import logging

import discord

LOGGER = logging.getLogger(__name__)

OP_LOG_FILENAME = 'op_log.csv'

class InviteClient(discord.Client):
    def __init__(self, roles):
        self.roles = roles
        # on_invite_create requires "invites" intent and manage_channels perm
        intents = discord.Intents(messages=False, guilds=True, members=True, invites=True)
        super().__init__(intents=intents)
        self.prep_op_log()

    def prep_op_log(self):
        try:
            with open(OP_LOG_FILENAME, 'r', newline='') as op_fp:
                reader = csv.reader(op_fp)
                for header in reader:
                    break
        except FileNotFoundError:
            header = ['guild_id', 'guild_name', 'op', 'link_code', 'link_nick',
                      'member', 'roles', 'notes']
            with open(OP_LOG_FILENAME, 'a', newline='') as op_fp:
                writer = csv.writer(op_fp)
                writer.writerow(header)

        self._op_log_headers = header

    def log_op(self, guild, op, link_code='', link_nick='', member=None, roles=None, notes=''):
        """Add an entry to the op log

        guild: Guild being changed
        op: operation
        link: invite link involved (short code)
        label: label of the invite link
        roles: roles assigned
        notes: extra info about what happened
        """
        data = dict(guild_id=guild.id, guild_name=guild.name, op=op,
                    link_code=link_code, link_nick=link_nick, member='(none)',
                    roles=roles, notes=notes)
        if member:
            data['member'] = f'{member.name}#{member.discriminator}'
        with open(OP_LOG_FILENAME, 'a', newline='') as op_fp:
            op_log = csv.DictWriter(op_fp, fieldnames=self._op_log_headers)
            op_log.writerow(data)

    async def on_ready(self):
        LOGGER.info(f'Logged on as {self.user}!')
        self.invites = {}
        for guild in self.guilds:
            await self.record_guild(guild)
        LOGGER.info(f'Known invites: {self.invites}')

    async def on_message(self, message):
        LOGGER.info(f'Message from {message.author}: {message.content}')

    async def on_guild_join(self, guild):
        """When joining a guild, record the invites"""
        LOGGER.info("joined guild: %s", guild)
        self.log_op(guild, 'guild_join')
        await self.record_guild(guild)

    # on_invite_create requires "invites" intent and manage_channels perm
    async def on_invite_create(self, invite):
        """When an invite is created, record it

        For simplicity, we just re-sync the whole guild."""
        LOGGER.info("invite created: %s", invite)
        self.log_op(invite.guild, 'invite_create', link_code=invite.code)
        await self.record_guild(invite.guild)

    async def on_member_join(self, member):
        """When a member joins, check if they used an invite"""
        guild_config = self.roles.get(member.guild.id, {})
        if not guild_config:
            LOGGER.warning("No config for guild %s (member %s), ignoring",
                           member.guild, member)
            return
        guild_invites = self.invites[member.guild.id]
        cur_invites = await member.guild.invites()
        invites = []
        for invite in cur_invites:
            old_uses = guild_invites.get(invite.id, None)
            if old_uses is None:
                LOGGER.warning("Invite %s (%d uses) didn't have old uses (recently created?)",
                               invite, invite.uses)
            elif invite.uses < old_uses:
                # This invite *lost* uses??
                LOGGER.warning("Invite %s used %d times now, previously %d (reduced uses)",
                               invite, invite.uses, old_uses)
            elif invite.uses == old_uses:
                # This invite wasn't used, ignore it
                pass
            elif invite.uses == old_uses + 1:
                # This invite was used once since we last checked
                invites.append(invite)
            elif invite.uses > old_uses + 1:
                # This invite was used multiple times since we last checked??
                LOGGER.warning("Invite %s used %d times now, previously %d (multiple uses)",
                               invite, invite.uses, old_uses)
                invites.append(invite)
            guild_invites[invite.id] = invite.uses
        LOGGER.info("New uses for guild %s now %s (invites used: %s)",
                    member.guild, guild_invites, invites)

        if len(invites) == 0:
            LOGGER.info("New member %s didn't use an invite", member)
            self.log_op(member.guild, 'join_noinvite', member=member)
        elif len(invites) == 1:
            invite = invites[0]
            invite_info = guild_config.get(invite.id, dict(nick='(none)', roles=[]))
            invite_nick = invite_info['nick']
            invite_roles = invite_info['roles']
            matching_role_objs = [member.guild.get_role(role) for role in invite_roles]
            role_objs = [role for role in matching_role_objs if role]
            role_match = (matching_role_objs == role_objs)
            if not role_match:
                LOGGER.warning("invite %s had bad roles: match=%s filtered=%s",
                               invites, matching_role_objs, role_objs)
            LOGGER.info("New member %s used one invite %s (%s), adding role %s (%s)",
                        member, invites, invite.id, invite_roles, role_objs)
            self.log_op(member.guild, 'join_invite', link_code=invite.code,
                        link_nick=invite_nick, roles=invite_roles, member=member,
                        notes=f'{role_match=}')
            if invite_roles:
                await member.add_roles(*role_objs, reason=f"auto-add from invite {invite}")
        elif len(invites) > 1:
            LOGGER.warning("New member %s used multiple invites %s, not adding roles",
                           member, invites)
            self.log_op(member.guild, 'join_multi_invite', member=member,
                        notes=f'{invites=}')


    async def record_guild(self, guild):
        """Record invite state of a guild"""
        LOGGER.info(f'Recording guild {guild} id={guild.id}')
        invites = await guild.invites()
        invite_dict = {}
        for invite in invites:
            LOGGER.info(f'  invite={invite} id={invite.id} uses={invite.uses}')
            invite_dict[invite.id] = invite.uses
        self.invites[guild.id] = invite_dict
        LOGGER.info(f'  roles={guild.roles}')
        self.log_op(guild, 'record_guild', notes=f'invites={invite_dict}')

if __name__ == '__main__':
    logging.basicConfig()
    LOGGER.setLevel(logging.DEBUG)
    with open('discord.roles.yaml', 'r') as role_fp:
        roles = yaml.safe_load(role_fp)
    client = InviteClient(roles)
    with open('discord.token', 'r') as token_fp:
        token = token_fp.read().strip()
    client.run(token)
